/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosdesarrollo;

/**
 *
 * @author 1DAM
 */
public class EntornosDesarrollo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Circulo c= new Circulo(4);
        System.out.println("El area del circulo es: ");
        c.area();
        System.out.println("El perimetro del circulo es: ");
        c.perimetro();
        Cuadrado cu=new Cuadrado(2);
        System.out.println("El area del cuadrado es: ");
        cu.area();
        System.out.println("El perimetro del cuadrado es: ");
        cu.perimetro();
        Rectangulo r=new Rectangulo(3,5);
        System.out.println("El area del rectangulo es: ");
        r.area();
        System.out.println("El perimetro del rectangulo es:  ");
        r.perimetro();
    }
    
}
