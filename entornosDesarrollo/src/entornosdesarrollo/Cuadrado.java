/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosdesarrollo;

/**
 *
 * @author 1DAM
 */
public class Cuadrado implements Figura{
    private int lado;
    @Override
    public void area() {
        System.out.println(lado*lado);
    }

    @Override
    public void perimetro() {
        System.out.println(lado*4);
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }

    public Cuadrado(int lado) {
        this.lado = lado;
    }
    
}
