/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornosdesarrollo;

/**
 *
 * @author 1DAM
 */
public class Circulo implements Figura{
    private int radio;
    @Override
    public void area() {
        System.out.println(3.1416 * Math.pow(radio,2));
    }

    @Override
    public void perimetro() {
        System.out.println(2*Math.PI*radio);
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Circulo(int radio) {
        this.radio = radio;
    }
    
}
