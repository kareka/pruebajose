/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repos1;

import java.util.Scanner;


public class Repos1 {

   
    public static void main(String[] args) {
        System.out.println("Introduce numero de filas");
        Scanner s=new Scanner(System.in);
        int nFilas=s.nextInt();
         System.out.println("Introduce numero de columnas");
        Scanner sc=new Scanner(System.in);
        int nColumnas=sc.nextInt();
        //Pedir por teclado nº filas y nº columnas
        int[][]matriz= new int [nFilas][nColumnas];
        //llame a las tres funciones y las imprime
        rellenar (matriz);
        imprimir (matriz);
        rellenarMult3(matriz);
        imprimir (matriz);
        rellenarAlReves(matriz);
         imprimir (matriz);
    }
    public static void imprimir (int[][]matriz){
         int n1=matriz.length;
         int n2=matriz[0].length;
          for(int i=0;i<n1;i++){
            for(int j=0;j<n2;j++){
                System.out.print(matriz[i][j]+" ");
             
            }
              System.out.println("");
        }
        System.out.println("********************************************"); 
    }
    public static void rellenar (int [][]matriz){
        //Rellene la matriz con numeros consecutivos.
        //Por ejemplo:
        //Si la matriz es de 3x4:
        //0123
        //4567
        //891011
        int n1=matriz.length;
        int n2=matriz[0].length;
        for(int i=0;i<n1;i++){
            for(int j=0;j<n2;j++){
                matriz[i][j]=j+matriz[i].length*i;
             
            }
            
        }
       
        
    } 
    public static void rellenarMult3(int[][]matriz){
        //Rellene la matriz con multiplos de tres.
        //Ej. con matriz 3x4:
        //0369
        //12151821
        //24273033
        int n1=matriz.length;
        int n2=matriz[0].length;
        //int contador=0;
        for(int i=0;i<n1;i++){
            for(int j=0;j<n2;j++){
                 matriz[i][j]=3*(j+matriz[i].length*i);
                //contador=contador+3;
             
            }
            
        }
    }
    public static void rellenarAlReves(int[][]matriz){
        int n1=matriz.length;
        int n2=matriz[0].length;
        //int contador=(n1*n2)-1;
        for(int i=0;i<n1;i++){
            for(int j=0;j<n2;j++){
                matriz[i][j]=((n1*n2)-1)-(i*n2)-j;
                //contador=contador-1;
             
            }
            
        }
    }
    }

