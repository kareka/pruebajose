/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramide;

/**
 *
 * @author 1DAM
 */
public class Piramide {

    /**
     * @param args the command line arguments
     */
     private static String imprimeCentrado(String[][] matriz) {
         String esp=" ";
        int mayor=0;
        int longitud=0;
        for(int i=0; i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
               longitud= longitud+ matriz[i][j].length();
                
             }
                  if(longitud>mayor){
                     mayor=longitud;
                  }  
                   longitud=0;
           
        }
         for(int i=0; i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
               longitud= longitud+ matriz[i][j].length();
            }
            int espacio =(mayor-longitud)/2;
            longitud=0;
            for (int j=0;j<espacio;j++){
                esp+=" ";
            }  
            for(int x=0;x<matriz[i].length;x++){
                esp+=matriz[i][x]+" ";
            }
            esp+="\n";
            
        }
         return esp;
     }
    public static void main(String[] args) {
        String[][]matriz;
        matriz= new String[4][];
        matriz[0]= new String[2];
        matriz[0][0]="dulce"; 
        matriz[0][1]="mantequilla";
        matriz[1]=new String[3];
        matriz[1][0]="carne";
        matriz[1][1]="lacteos";        
        matriz[1][2]="huevos";
        matriz[2]=new String[4];
        matriz[2][0]="fruta";
        matriz[2][1]="verdura";
        matriz[2][2]="hortaliza";
        matriz[2][3]="legumbres";
        matriz[3]=new String[4];
        matriz[3][0]="pan";
        matriz[3][1]="arroz";
        matriz[3][2]="trigo";
        matriz[3][3]="cereales";
        
     String resultado = imprimeCentrado(matriz);
        System.out.println(resultado);
        
    }

   
    }

