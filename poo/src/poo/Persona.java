/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author 1DAM
 */
class Persona {
    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca misVacas[];
    
   public Persona(String no,String ap,int ed,float al,String pu){
       this.nombre=no;
       this.apellidos=ap;
       this.edad=ed;
       this.altura=al;
       this.puesto=pu;
       if(this.puesto.equals(Constantes.gerencia)){
           this.sueldo=2000;
       }else if(this.puesto.equals(Constantes.ordeño)){
           this.sueldo=1000;
       }else{
           this.sueldo=750;
       }
       this.misVacas=new Vaca[3];
   } 
    
   public  String imprimePersona(){
        String salida= this.nombre+" : "+this.apellidos+" : "+this.edad+" : "+this.altura+"|Mis vacas: ";
        for(int i=0; i<misVacas.length;i++){
            if(misVacas[i]!=null){
             salida+="\n\t"+misVacas[i].imprimeVaca();
            }
        }
        return salida;
    }
    /**
    * @param int la edad co la que vamos a comparar
    *Dice si la persona que llama a la funcion es mayoer de cierta edad
    * @return true si la persona es mayor, false si es menor o igual
    */
   public boolean esMayorDe(int edad){
       return this.edad>edad;
   }
   public String cuantoGana(float sueldo){
       return "Gana"+" "+this.sueldo+" "+" €";
   }
   public float proporcionSueldo(Persona p2){
       return (p2.sueldo/this.sueldo);
   }
   public float ajusteSueldoA(Persona p){
       if ((this.puesto.equals(Constantes.ordeño))&&(p.puesto.equals(Constantes.gerencia))){
           this.sueldo= (p.sueldo)-0.5f*(p.sueldo);
       }else 
       if ((this.puesto.equals(Constantes.ordeño))&&(p.puesto.equals(Constantes.carniceria))){
           this.sueldo= (p.sueldo)+0.25f*(p.sueldo);
       }else
       if((this.puesto.equals(Constantes.carniceria))&&(p.puesto.equals(Constantes.ordeño))){
           this.sueldo=(p.sueldo)-0.25f*(p.sueldo);
       }else
       if((this.puesto.equals(Constantes.carniceria))&&(p.puesto.equals(Constantes.gerencia))){
           this.sueldo=(p.sueldo)-0.75f*(p.sueldo);
       }else
       if((this.puesto.equals(Constantes.gerencia))&&(p.puesto.equals(Constantes.ordeño))){
           this.sueldo=(p.sueldo)+0.50f*(p.sueldo);
       }else
        if((this.puesto.equals(Constantes.gerencia))&&(p.puesto.equals(Constantes.carniceria))){
           this.sueldo=(p.sueldo)+0.75f*(p.sueldo);
       }else{
            this.sueldo=p.sueldo;
        }
       return this.sueldo;
   }
   
    public void quedarmeVacas(Vaca[]vac){
     this.misVacas=new Vaca[vac.length];
     int contador=0;
     for (int i=0;i<misVacas.length;i++){
         if(this.puesto.equals(vac[i].funciones)){
         this.misVacas[contador]=vac[i];
         contador++;
          }
     }
    
 } 
     public void muestraVacas(){
         
         System.out.println(" Las vacas de "+this.nombre); 
         for(int i=0;i<misVacas.length;i++){
            if(this.misVacas[i]==null){
                break;
            }else{
                System.out.println(this.misVacas[i].nombre+" ");
            }
         }
     }
}
