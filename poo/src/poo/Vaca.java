/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author 1DAM
 */
public class Vaca {
    String nombre;
    int numeroSerie;
    String funciones;
    float peso;
    int edad;
    float[]litros;
/*
 Construye el objeto vaca con sus datos
    *n el nombre
    *ns el numero de serie
    *p el peso
    *e la edad
    *f la funcion
    */    
public  Vaca(String n, int ns, float p, int e,String f){
    this.nombre=n;
    this.numeroSerie=ns;
    this.peso=p;
    this.edad=e;
    this.funciones=f;
    this.litros=new float[15];
}    
public Vaca(String n, int ns, float p, int e){
    this.nombre=n;
    this.numeroSerie=ns;
    this.peso=p;
    this.edad=e;
    this.asignaFuncion();
    this.litros=new float[15];
}    
    
public void asignaFuncion(){
    if(this.peso>200){
        this.funciones=Constantes.carniceria;
    }else{
        this.funciones=Constantes.ordeño;
    }
    
}
public String imprimeVaca(){
    String salida=this.nombre+" "+this.numeroSerie+" "+this.peso+" "+this.edad+" "+this.funciones+" ";
    for(int i=0;i<litros.length;i++){
        salida+=this.litros[i]+",";
    }
    return salida;
}
}





