/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolgenealogico;

/**
 *Define personas
 * @author 1DAM
 */
// Tarea: con esta clase en un main, crear tres generaciones de personas
//Abuela3 Abuelo3 Abuela2 Abuelo2 Abuela1 Abuelo1
//   |      |       |        |      |       |
//  ----------     -----------     -----------
//      |           |       |           |
//    Madre2     Padre2   Madre1      Padre1
//   |              |       |           |
//  ------------------    ------------------
//   |              |             |
// Hija2          Hijo2         Hijo1
//Construir este árbol genealógico usando la clase Persona
//y hacer sout(hijo1.devuelveMadre().devuelvePareja().devuelvePadre())
public class Persona {
//Añadir a cada persona un padre y una madre
//Añadir a cada persona una pareja
//Añadir a cada persona un array de hijos    
    Persona padre;
    Persona madre;
    Persona pareja;
    
    String nombre;
    String primerApellido;
    String segundoApellido;
    boolean sexo;//true femenino y false masculino
    Persona[]hijos;
    
     public Persona(String n,String a,String a2,boolean s,Persona p,Persona m,Persona pa){
         this.nombre=n;
         this.primerApellido=a;
         this.segundoApellido=a2;
         this.sexo=s;
         this.madre=m;
         this.padre=p;
         this.pareja=pa;
         this.hijos=hijos;
         
         
         
     }
     public Persona devuelveMadre(){
         return madre;
     }
     public Persona devuelvePareja(){
         return pareja;
     }
     public Persona devuelvePadre(){
         return padre;
     }
}
